---
layout: handbook-page-toc
title: "Public Sector"
description: "Public Sector at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Government Customers/Contracting

[Please see the Government Customers/Contracting Doc](https://docs.google.com/document/d/1u81wxeSOqkIRWgq53YTWoFhWjqtEVnGC0NEgno-yEJc/edit#bookmark=id.fqk8j57a9uja)
